#include "Game.h"

Game::Game()
{
	renderWindow_.create(sf::VideoMode(800, 600), "Dodge and Weave");
	renderWindow_.setFramerateLimit(60);
	/*sf::View view = renderWindow_.getDefaultView();
	view.zoom(2.0f);
	renderWindow_.setView(view);*/
}

Game::~Game()
{
	//dtor
}

void Game::initGame()
{
	player_ = new Player((float)renderWindow_.getSize().x / 2, (float)renderWindow_.getSize().y / 2, &renderWindow_);
	setGameOver(false);
}

void Game::startGame()
{
	bool showMenu = true;
	sf::Font textFont;
	textFont.loadFromFile("assets/arial.ttf");
	sf::Text menuText("Avoid the red squares!!\n--WASD to move.\n--Click enemy to kill them.\nPress Space to start.", textFont, 50);
	while (showMenu)
	{
		renderWindow_.clear();
		renderWindow_.draw(menuText);
		renderWindow_.display();
		menuPoll(&showMenu);
	}
	renderWindow_.clear();
	initGame();
	gameLoop();
}

void Game::menuPoll(bool* menu)
{
	sf::Event event;

	while (renderWindow_.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			//renderWindow_.close();
			break;
		case sf::Event::KeyPressed:
			if (event.key.code == sf::Keyboard::Space)
			{
				*menu = false;
			}
			break;
		default:
			break;
		}
	}

}

void Game::gameLoop()
{
	sf::Clock spawnTimer;
	sf::Time timeOne;
	while (renderWindow_.isOpen())
	{
		pollEvent();

		if (!isGameOver())
		{
			checkInput();

			updateAI();

			checkCollision();
		}

		updateWindow();

		timeOne = spawnTimer.getElapsedTime();
		if (timeOne.asSeconds() >= 1.0f && !isGameOver())
		{
			spawnTimer.restart();
			spawnEnemy();
		}

		if (isGameOver())
			deleteEnemys();
	}
	delete player_;
}

void Game::pollEvent()
{
	sf::Event event;

	while (renderWindow_.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			renderWindow_.close();
			break;
		case sf::Event::KeyPressed:
			if (isGameOver() && event.key.code == sf::Keyboard::Space)
			{
				setGameOver(false);
				player_->resetAliveTimer();
			}
			break;
		case sf::Event::MouseMoved:
		{
			float angle = atan2(event.mouseMove.y - player_->getPosition().y, event.mouseMove.x - player_->getPosition().x);
			angle *= (180/M_PI);
			//if (angle < 0)
				//angle = 360 - (-angle);
			player_->setRotation(angle);
			break;
		}
		case sf::Event::MouseButtonPressed:
			if (event.mouseButton.button == sf::Mouse::Left && !isGameOver())
			{
				sf::Vector2i mousePos = sf::Mouse::getPosition(renderWindow_);
				for (int i = 0; i < enemys_.size(); i++)
				{
					if (enemys_[i]->getGlobalBounds().contains((sf::Vector2f) mousePos))
					{
						player_->killedEnemy();
						killEnemy(enemys_[i]);
					}
				}
			}
			break;
		default:
			break;
		}
	}
}

void Game::checkInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		player_->move(0.0f, -1.0f*player_->getMovementSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		player_->move(0.0f, 1.0f*player_->getMovementSpeed());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		player_->move(-1.0f*player_->getMovementSpeed(), 0.0f);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		player_->move(1.0f*player_->getMovementSpeed(), 0.0f);
	}
}

void Game::updateAI()
{
	for (int i = 0; i < enemys_.size(); i++)
	{
		sf::Vector2f direction = enemys_[i]->getNewDirection(player_->getPosition());
		enemys_[i]->move(direction.x*enemys_[i]->getMovementSpeed(), direction.y*enemys_[i]->getMovementSpeed());
	}
}

void Game::checkCollision()
{
	sf::FloatRect playerBounds = player_->getGlobalBounds();
	std::vector<sf::FloatRect> enemyBounds;

	for (int i = 0; i < enemys_.size(); i++)
		enemyBounds.push_back(enemys_[i]->getGlobalBounds());

	for (int i = 0; i < enemyBounds.size(); i++)
	{
		if (playerBounds.intersects(enemyBounds[i]))
		{
			setGameOver(true);
			player_->resetKillCount();
			player_->resetAliveTimer();
		}

		for (int j = i+1; j < enemyBounds.size(); j++)
		{
			if (enemyBounds[i].intersects(enemyBounds[j]))
			{
				sf::Vector2f direction = enemys_[i]->getNewDirection(enemys_[j]->getPosition());
				enemys_[i]->move(-(direction.x*enemys_[i]->getMovementSpeed()), -(direction.y*enemys_[i]->getMovementSpeed()));
			}
		}
	}
}

void Game::updateWindow()
{
	renderWindow_.clear();

	if (isGameOver() == false)
	{
		renderWindow_.draw(*player_);
		for (int i = 0; i < enemys_.size(); i++)
		{
			renderWindow_.draw(*enemys_[i]);
		}

		renderWindow_.draw(player_->killCount());
		renderWindow_.draw(player_->aliveTime());
	}
	else
	{
		sf::Font textFont;
		textFont.loadFromFile("assets/arial.ttf");
		sf::Text gameOverText("Game Over!!\nPress Space to restart.", textFont, 50);
		renderWindow_.draw(gameOverText);
	}

	renderWindow_.display();
}

void Game::spawnEnemy()
{
	enemys_.push_back(new Enemy);
}

void Game::killEnemy(Enemy* enemy)
{
	std::vector<Enemy*>::iterator it;
	it = std::find(enemys_.begin(), enemys_.end(), enemy);
	if (it != enemys_.end())
	{
		delete *it;
		enemys_.erase(it);
	}
}

void Game::setGameOver(bool state)
{
	gameOver_ = state;
}

bool Game::isGameOver()
{
	return gameOver_;
}

void Game::deleteEnemys()
{
	for (int i = 0; i < enemys_.size(); i++)
	{
		delete enemys_[i];
		enemys_.erase(enemys_.begin()+i);
	}
}
