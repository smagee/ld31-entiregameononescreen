#include "Entity.h"

Entity::Entity()
{
	//ctor
}

Entity::~Entity()
{
	//dtor
}

sf::Vector2f Entity::getPosition()
{
	return body_.getPosition();
}

void Entity::setPosition(sf::Vector2f pos)
{
	body_.setPosition(pos);
}

void Entity::move(sf::Vector2f offset)
{
	body_.move(offset);
}

void Entity::move(float x, float y)
{
	body_.move(x, y);
}

void Entity::setRotation(float angle)
{
	return body_.setRotation(angle);
}

float Entity::getRotation()
{
	return body_.getRotation();
}

sf::FloatRect Entity::getGlobalBounds()
{
	return body_.getGlobalBounds();
}

float Entity::getMovementSpeed()
{
	return movement_;
}
