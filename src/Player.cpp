#include "Player.h"

Player::Player(float posX, float posY, sf::RenderWindow* window)
{
	textFont_.loadFromFile("assets/arial.ttf");

	killCount_.setFont(textFont_);
	aliveTime_.setFont(textFont_);
	//aliveTime_.setOrigin(aliveTime_.getOrigin().x + aliveTime_.getPosition().x, aliveTime_.getOrigin().x + aliveTime_.getPosition().x);
	aliveTime_.setPosition(0, killCount_.getPosition().y);

	body_.setSize(sf::Vector2f(12, 12));
	body_.setOrigin((body_.getSize().x / 2), (body_.getSize().y / 2));
	body_.setPosition(posX, posY);
	body_.setFillColor(sf::Color::Green);

	movement_ = 10.0f;
	kills_ = 0;
	aliveTimer_.getElapsedTime();
}

Player::~Player()
{

}

void Player::killedEnemy()
{
	kills_++;
}

float Player::getAliveTime()
{
	sf::Time aliveTime = aliveTimer_.getElapsedTime();
	return aliveTime.asSeconds();
}

sf::Text Player::killCount()
{
	std::stringstream ss;
	ss << "Kills: ";
	ss << kills_;

	killCount_.setString(ss.str());
	return killCount_;
}

sf::Text Player::aliveTime()
{
	std::string time("Time: ");

	std::stringstream ss;
	ss << getAliveTime();
	time += ss.str();

	aliveTime_.setString(time);
	aliveTime_.setPosition(0, killCount_.getCharacterSize());
	return aliveTime_;
}

void Player::resetKillCount()
{
	kills_ = 0;
}

void Player::resetAliveTimer()
{
	aliveTimer_.restart();
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(body_, states);
	return;
}
