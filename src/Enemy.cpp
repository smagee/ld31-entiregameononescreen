#include "Enemy.h"

Enemy::Enemy()
{
	srand (time(NULL));

	int spawnCorner = rand() % 4;

	body_.setSize(sf::Vector2f(25, 25));
	body_.setOrigin((body_.getSize().x / 2), (body_.getSize().y / 2));
	body_.setFillColor(sf::Color::Red);

	switch (spawnCorner)
	{
	case 0:
		body_.setPosition(0,0);
		break;
	case 1:
		body_.setPosition(800, 0);
		break;
	case 2:
		body_.setPosition(0,600);
		break;
	case 3:
		body_.setPosition(800, 600);
		break;
	}

	movement_ = rand() % 8 +1;
}

Enemy::~Enemy()
{
	//dtor
}

sf::Vector2f Enemy::getNewDirection(sf::Vector2f otherPos)
{
	sf::Vector2f myPos = getPosition();
	sf::Vector2f direction = sf::Vector2f(otherPos.x - myPos.x, otherPos.y - myPos.y);
	float hyp = sqrtf(direction.x*direction.x+direction.y*direction.y);
	direction.x /= hyp;
	direction.y /= hyp;
	return direction;
}

void Enemy::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(body_, states);
	return;
}
