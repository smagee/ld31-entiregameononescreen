#include <SFML/Graphics.hpp>

#include "Game.h"

int main()
{
	Game mainGame;

	mainGame.startGame();

    return 0;
}
