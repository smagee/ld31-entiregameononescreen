#ifndef ENEMY_H
#define ENEMY_H

#include <cstdlib>
#include <cmath>

#include "Entity.h"

class Enemy : public sf::Drawable, public Entity
{
	public:
		Enemy();
		virtual ~Enemy();

		sf::Vector2f getNewDirection(sf::Vector2f);
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
};

#endif // ENEMY_H
