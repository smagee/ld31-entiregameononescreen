#ifndef GAME_H
#define GAME_H

#include <cmath>
#include <ctime>
#include <iostream>
#include <vector>
#include <algorithm>

#include <SFML/Graphics.hpp>

#include "Player.h"
#include "Enemy.h"

class Game
{
	public:
		Game();
		virtual ~Game();

		void initGame();
		void startGame();
		void gameLoop();
		void pollEvent();
		void checkInput();
		void updateAI();
		void checkCollision();
		void updateWindow();
		void menuPoll(bool*);

		void spawnEnemy();
		void killEnemy(Enemy*);

		void setGameOver(bool);
		bool isGameOver();

		void deleteEnemys();
	protected:
	private:
		sf::RenderWindow renderWindow_;
		Player* player_;
		std::vector<Enemy*> enemys_;

		bool gameOver_;
};

#endif // GAME_H
