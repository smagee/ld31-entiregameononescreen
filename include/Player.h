#ifndef PLAYER_H
#define PLAYER_H

#include <sstream>
#include <iostream>
#include <string>

#include "Entity.h"

class Player : public sf::Drawable, public Entity
{
	public:
		Player(float, float, sf::RenderWindow*);
		virtual ~Player();

		void killedEnemy();

		float getAliveTime();
		sf::Text killCount();
		sf::Text aliveTime();

		void resetKillCount();
		void resetAliveTimer();
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		int kills_;
		sf::Clock aliveTimer_;

		sf::Font textFont_;
		sf::Text killCount_;
		sf::Text aliveTime_;
};

#endif // PLAYER_H
