#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

class Entity
{
	public:
		Entity();
		virtual ~Entity();

		sf::Vector2f getPosition();
		void setPosition(sf::Vector2f);
		void move(sf::Vector2f);
		void move(float, float);
		void setRotation(float);
		float getRotation();
		sf::FloatRect getGlobalBounds();

		float getMovementSpeed();
	protected:
		sf::RectangleShape body_;
		float movement_;
	private:
};

#endif // ENTITY_H
